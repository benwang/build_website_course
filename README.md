# Build a Website Course

## Commonly used lines
To replace all kramdown target="" links (for opening hyperlinks in new tabs instead of current tab), do this regex find and replace:

  Find: `\[(.*?)\]\((.*?)\)\{:target="_blank"\}`
  Replace: `<a href="$2" target="_blank">$1</a>`

