---
layout: post
title:  "Week 1 - Introduction"
date:   2020-06-07
categories: syllabus
---

## Before class

- Install <a href="https://atom.io/" target="_blank">Atom text editor</a>
- Come up with a website idea and 3-4 web pages. For example:
  - For a personal/portfolio website, the pages might be:
    - Home page/projects
    - About me
    - Contact info
    - blog
  - For a TV show reviews/rankings website, the pages could be:
    - a home page explaining the website, shows rankings and links to each TV show
    - a page for each TV show

## Meeting info

We will meet on **Thursdays at 8pm CT/9pm ET** and last for approximately an hour. <b>Link to the class <a href="{{ site.zoomlink }}" target="_blank">Zoom meeting</a></b>.

## Objectives

- Understand what happens when you open a website
  - Server vs client
  - HTML - language of the internet
  - URLs, IP addresses, and DNSes
  - Later on: Javascript, databases, ...
- Basic sections - Header, Body, Footer
- Basic text styles - heading, paragraph, unordered list, ordered list
- Links
- Pictures

## During class

- How do you imagine websites are made?
- What do you want to learn? Is there anything in specific?
- What website would you like to make?
- Create a `Week_1` folder
- Start with blank text file, add HTML tags, header text, paragraph
- Introduce <a href="https://www.w3schools.com/html/" target="_blank">W3 schools website</a>
- Questions? Things you want to learn to do?

Sample course materials for the Week 1 class are available <a href="https://gitlab.com/benwang/build_website_course/-/tree/master/Course_Materials/Week_1" target="_blank">here</a>.

## Homework

1) Re-open up your index.html in Atom.
2) Add a heading by adding `<h1>Your Heading</h1>` somewhere in the `<body>` section
3) Add another paragraph below the new heading with some text.
4) Open the web page in your web browser and make sure it worked!
5) **Advanced**: Try out the other tags from my example index.html file (link). You can download both index.html and about.html to the same folder and open them in your web browser. These tags include:

    - `<ul></ul>`: Unordered List (think bullet points in Microsoft Word). Each bullet is denoted with `<li></li>`. For example:
      ```HTML
        <ul>
          <li>first item</li>
          <li>second item</li>
        </ul>
      ```
    - `<ol></ol>`: Ordered list. Just like `<ul>`, but these are numbered bullets
    - `<a href="insert some URL here">Link name</a>`: Links to another webpage or website. To link to your own page, simply place the other html file in the same folder and put the name of the file here. For example, my example index.html links to "about.html".
    - You can find a ton of HTML reference here:  https://www.w3schools.com/  
