---
layout: post
title:  "Week 2 - Styling and CSS"
date:   2020-06-07
categories: syllabus
---

## Meeting info

We will meet on **Thursdays at 8pm CT/9pm ET** and last for approximately an hour. <b>Link to the class <a href="{{ site.zoomlink }}" target="_blank">Zoom meeting</a></b>.

## Objectives

- Introduce remaining basic elements - lists, images, links
- Text styles (bold, italics, underline)
- Text and image alignment (center, left, right)
- Background color
- Font color
- Make clear that there are much easier ways to make websites

## During class

- Review homework
- Continue from last week - add lists, images, and links
- Create 1 additional page so that the home page can link to it
- Go over the topics in the objectives (alignment, background color, font color)
- Let style section get long, introduce CSS sheets
  - Given 2 or 3 CSS files, change the theme of the website instantly
- Questions? Things you want to learn to do?

## Outcome

Your website might look something like this: <a href="http://217.tplinkdns.com/Course_Materials/Week_2/index.html" target="_blank">Ben's Week 2 Example</a>

This week's course materials available <a href="https://gitlab.com/benwang/build_website_course/-/tree/master/Course_Materials/Week_2" target="_blank">here</a>

## Homework

- Create additional pages from original idea. Add titles to them and a each page should have a link back to the home page.
- Find and email me:
  - a website you really like
  - a website you find confusing

<br>

---

<br>

## Week 2 Material

### Lists

There are two types of lists available - ordered and unordered lists.

#### Unordered Lists

Unordered lists are typical "bullets", like in Word documents.

```HTML
<p>Ingredients:</p>
<ul>
  <li>Sugar</li>
  <li>Butter</li>
  <li>Flour</li>
</ul>
```

Renders the following:

---

<p>Ingredients:</p>
<ul>
  <li>Sugar</li>
  <li>Butter</li>
  <li>Flour</li>
</ul>

---

<br>

#### Ordered Lists

Ordered lists are numbered bullets.

```HTML
<p>Steps:</p>
<ol>
  <li>Combine ingredients</li>
  <li>Mix</li>
  <li>Bake</li>
</ol>
```

Renders the following:

---

<p>Steps:</p>
<ol>
  <li>Combine ingredients</li>
  <li>Mix</li>
  <li>Bake</li>
</ol>

---

<br>

### Breaks

If you want an empty line in between text (because hitting enter or adding a blank line in your text doesn't work), use the `<br>` tag. For example:
```html
<p>
  Some text here
  <br>
  but I want a 2nd line
  and simply putting a new line here doesn't work!
</p>
```

Renders:

---


<p>
Some text here
  <br>
  but I want a 2nd line
  and simply putting a new line here doesn't work!
</p>

---

<br>

### Images

Add images with the `<img>` tag. The `alt` field is for alternative text - text that shows up when the image is not available. The `src` field can either contain the filename of a picture in the same directory as your index.html *or* be the URL to an image.

```html
<img src="filename.jpg" alt="alternative text">
```

For example, if I wanted to include the picture located at __, I could do:

```html
<img src="http://cliparts.co/cliparts/6cr/6x4/6cr6x4xxi.gif" alt="Person on computer">
```

Which renders:

---

<img src="http://cliparts.co/cliparts/6cr/6x4/6cr6x4xxi.gif" alt="Person on computer">

---

<br>

Also, suppose I got the URL wrong so the image doesn't exist, now you'll see the alt text:

```html
<img src="http://cliparts.co/cliparts/6cr/6x4/6cr6x4xxi_OOPS.gif" alt="Person on computer">
```

Which renders:

---

<img src="http://cliparts.co/cliparts/6cr/6x4/6cr6x4xxi_OOPS.gif" alt="Person on computer">

---

<br>

Additionally, you can specify the size of the image too. If interested, more information is available here: https://www.w3schools.com/html/html_images.asp


### Links
Add links with the `<a href="URL">link text</a>` tag.

```html
<a href="https://google.com">Take me to Google</a>
```

Renders the following:

---

<a href="https://google.com">Take me to Google</a>

---

<br>

### Your turn

Now that you've seen the elements we are covering today, go ahead and try them out on your website! Follow these steps:

1) Take your code from last week and place it in a new folder, called `Week_1`, if you haven't done so already.

    <details>
      <summary>Click to see how on Windows</summary>

    1) Go to your Desktop. To do this, open **File Explorer** by going to the **Start menu** and searching for "file explorer".

        ![File explorer]({{ site.baseurl }}/images/Week_2/Week_2-File_explorer_Windows.png)

    2) Once opened, go to your Desktop by clicking on **Desktop** under 

        ![Desktop]({{ site.baseurl }}/images/Week_2/Week_2-File_explorer_Windows-Desktop.png)

    3) Now right click somewhere in the white space in the Desktop folder. Click **New**, then **Folder**.

        ![Desktop new folder]({{ site.baseurl }}/images/Week_2/Week_2-File_explorer_Windows-Desktop-new_folder.png)

    4) Name the folder "Week_1"

        ![Week 1 folder]({{ site.baseurl }}/images/Week_2/Week_2-File_explorer_Windows-Desktop-Week_1_folder.png)


    5) Click and drag the `index.html` file from last week into the folder

        ![Week 1 drag]({{ site.baseurl }}/images/Week_2/Week_2-File_explorer_Windows-Desktop-Week_1_folder-drag.png)
    </details>

    
    <details>
      <summary>Click to see how on Mac</summary>
    
    1) From the **Dock**, click Finder.

        ![Finder on Dock]({{ site.baseurl }}/images/Week_2/Week_2-Finder_Mac.png)

    2) Click on Desktop on the left.

        ![Finder Desktop]({{ site.baseurl }}/images/Week_2/Week_2-Finder_Mac-opened.png)

    2) Right click (or 2-finger click) so the menu comes up. Click **New Folder**.

        ![New folder]({{ site.baseurl }}/images/Week_2/Week_2-Finder_Mac-new_folder.png)

    3) Name the folder "Week_1"

        ![Week_1 folder]({{ site.baseurl }}/images/Week_2/Week_2-Finder_Mac-Week_1_folder.png)

    4) Click and drag the `index.html` file from last week into the folder

        ![Drag in file]({{ site.baseurl }}/images/Week_2/Week_2-Finder_Mac-Week_1_folder-drag.png)

    </details>

2) Make a copy of your `Week_1` folder by:
    <details>
      <summary>Click to see how on Windows</summary>

    1) Right-clicking on it
    2) Clicking **Copy**
    3) Right-clicking in some empty space in the window (or on your Desktop)
    4) Clicking **Paste**
    5) Right-click the new folder, click **Rename**. Rename the new folder to "Week_2"
    
    </details>
    <details>
      <summary>Click to see how on Mac</summary>

    1) Right-clicking (or 2-finger click) on it
    2) Clicking **Duplicate**
    3) Right-click (or 2-finger click) the new folder, click **Rename**. Rename the new folder to "Week_2"

    </details>

3) Open up the `Week_2/index.html` file in Atom. Add an ordered or unordered list to the `<body>` section.
4) Now go to images.google.com and search for some image to place on your home page (index.html). Right click the picture, click "Save image as...", and save the image to the `Week_2` folder on your desktop.
5) Next, create a second page. In Atom, go to **File** > **New**. Save as `about.html`, also in the `Week_2` folder.
6) Type in the following in `about.html`:

    ```html
    <html>
      <head>
        <title>About</title>
      </head>
      <header>
        <h2>Header</h2>
        <ul>
          <li><a href="index.html">Home</a></li>
          <li><a href="about.html">About</a></li>
        </ul>
      </header>
      <body>
        <h1>About</h1>
      </body>
    </html>
    ```

7) Go back to index.html and make the beginning of the document similar:

    ```html
    <html>
      <head>
        <title>Home</title>
      </head>
      <header>
        <h2>Header</h2>
        <ul>
          <li><a href="index.html">Home</a></li>
          <li><a href="about.html">About</a></li>
        </ul>
      </header>
      <body>
        ...
      </body>
    </html>
    ```

If you get stuck, feel free to look at my example code:
- <a href="https://gitlab.com/benwang/build_website_course/-/raw/master/Course_Materials/Week_2/index.html" target="_blank">index.html</a>
- <a href="https://gitlab.com/benwang/build_website_course/-/raw/master/Course_Materials/Week_2/about.html" target="_blank">about.html</a>

---

<br>

## Colors & Text Styles

### Text Emphasis

```html
<p><strong>Bold</strong></p>
<p><em>Italics</em> or <i>Italics</i></p>
<p><u>Underlined</u></p>
```
Renders:

---

<p><strong>Bold</strong></p>
<p><em>Italics</em> or <i>Italics</i></p>
<p><u>Underlined</u></p>

---

<br>

### Font Color

Font color can be set using the style property:

```html
<p style="color:red">Some text</p>
```

Renders the following:

---

<p style="color:red">Some text</p>

---

<br>

You can make your own custom colors too. Especially with computers, you may have heard of RGB before. RGB stands for Red - Green - Blue, and are the primary colors in the additive color model (when light is combined) and are the basis behind color used in screens. Looking very closely at your monitor, you might be able to see that each pixel is actually made of 3 small lights. By changing the amount of red, green, and blue light present, any arbitrary color can be made.

![pixels](https://upload.wikimedia.org/wikipedia/commons/d/de/Closeup_of_pixels.JPG)

You can specify RGB colors by doing the following, where for each color, 0 is the darkest and 255 is the brightest:

```html
<p style="color:rgb(179, 25, 255);">Some text</p>
```

Which renders:

---

<p style="color:rgb(179, 25, 255);">Some text</p>

---

<br>

### Background colors

Similarly, you can do background colors with:

```html
<p style="background-color:rgb(179, 25, 255);">Some text</p>
```

Which renders:

---

<p style="background-color:rgb(179, 25, 255);">Some text</p>

---

<br>

Here's a helpful tool for creating/picking colors: https://www.w3schools.com/colors/colors_rgb.asp

If you're interested, more on colors here: https://www.w3schools.com/html/html_colors.asp

<br>

### Font Size

See: https://www.w3schools.com/tags/tag_font.asp

### Your Turn Again

Now give some of these a try!

---

<br>

## A Sneak Peek...

By now, you can see how making websites purely in HTML is a long and tedious process. *Simple* websites from the 90s no longer seem so simple, huh?

For example:
![old apple website]({{ site.baseurl }}/images/Week_2/website-design.jpg "https://www.wired.com/images_blogs/gadgetlab/2013/01/website-design.jpg")

But this isn't what modern websites look like, and not many people are going to spend that much time working on details like formatting when there are much easier ways.

For example, this course website is written completely in something called Markdown and the hosting is done on a service by GitLab. Other modern website building services include:

<center>

| Service name                                                   | Paid?       | language / renderer         |
|----------------------------------------------------------------|-------------|-----------------------------|
| <a href="https://docs.gitlab.com/ee/user/project/pages/" target="_blank">GitLab Pages</a> | Free        | Markdown & Jekyll           |
| <a href="https://pages.github.com/" target="_blank">GitHub Pages</a>                      | Free        | Markdown & Jekyll           |
| <a href="https://gsuite.google.com/products/sites/" target="_blank">Google Sites</a>      | Free        | Graphical editor            |
| <a href="https://www.squarespace.com/" target="_blank">Squarespace</a>                    | Paid        | Graphical editor            |
| <a href="https://wordpress.com/" target="_blank">Wordpress</a>                            | Free & Paid | Markdown / Graphical editor |
| <a href="https://wix.com" target="_blank">Wix</a>                                         | Free & Paid | Graphical editor            |
| <a href="https://www.webs.com/" target="_blank">Webs.com</a>                              | Free & Paid | Graphical editor            |

</center>


### Markdown?

The concept is that documents can be easy to read both as plain text and also be rendered. For example, a basic document:

```markdown
Title
=====

Subheading
----------

A list:
- sugar
- butter
- flower

Numbered list:
1) add ingredients
2) bake
```

Renders as:

---

Title
=====

Subheading
----------

A list:
- sugar
- butter
- flower

Numbered list:
1) add ingredients
2) bake

---
