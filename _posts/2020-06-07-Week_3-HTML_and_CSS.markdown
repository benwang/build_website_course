---
layout: post
title:  "Week 3 - HTML and CSS"
date:   2020-06-07
categories: syllabus
---

## Meeting info

<b>Link to the class <a href="{{ site.zoomlink }}" target="_blank">Zoom meeting</a></b>.

## Objectives

- Recap from Weeks 1 and 2
- Introduce CSS
- Introduce Javascript

## During class

- Go over <a href="https://gitlab.com/benwang/build_website_course/-/raw/master/Course_Materials/Week_3/Week_3_Slides.pdf?inline=false" target="_blank">Week 3 slides</a>
- Do CSS activity
- Pull up one of the production websites, show the View page source and Inspect element features
- Questions? Things you want to learn to do?

## Homework

- Make sure you have a Google account for next week

<br>

---

<br>

## Week 3 Material

- Full Week 3 materials: <a href="https://gitlab.com/benwang/build_website_course/-/tree/master/Course_Materials/Week_3" target="_blank">here</a>
- Week 3 full examples:
  - <a href="http://217.tplinkdns.com/Course_Materials/Week_3/no_style/index.html" target="_blank">Website with no CSS applied</a>
  - <a href="http://217.tplinkdns.com/Course_Materials/Week_3/style1/index.html" target="_blank">Website with style 1 applied</a>
  - <a href="http://217.tplinkdns.com/Course_Materials/Week_3/style2/index.html" target="_blank">Website with style 2 applied</a>
- Slides: <a href="https://gitlab.com/benwang/build_website_course/-/raw/master/Course_Materials/Week_3/Week_3_Slides.pdf?inline=false" target="_blank">here</a>

### Week 2 Wrap-up

Homework last week was to find sites that you liked and disliked:

| Liked website | Reason Liked | Disliked | Reason Disliked |
|-|-|-|-|
| https://smittenkitchen.com/ |  | SONA |  |
| https://www.hgtv.com/outdoors/flowers-and-plants/trees-and-shrubs/hydrangea-cheat-sheet | Liked this website better. More interesting and creative. | https://www.realsimple.com/home-organizing/gardening/outdoor/hydrangea-care  |  This website was a little boring but easy to follow. | 
| https://www.youtube.com/results?search_query=vivian+chiu |  |  |  |
| Google Earth |  | Government websites |  |


### Comments in HTML

Sometimes you want to add text to your HTML source files to explain what you are doing. Other times you might have code you wish to disable but want to keep around. For this purpose, there are comments in HTML, in the format `<!--YOUR COMMENT HERE-->`.

For example, in my Week 3 example for the CSS Activity (next), you'll see that I have both style1 and style2 in my `<head>` section, however both are commented out.

```html
<html>
  <head>
    <title>Welcome to my website</title>
    <!-- <link rel="stylesheet" href="style1.css"> -->
    <!-- <link rel="stylesheet" href="style2.css"> -->
  </head>
  <header>
  ...
</html>
```

### CSS Activity

1) Create a copy of your `Week_2` folder and name it `Week_3`
2) Download the following CSS files to your `Week_3` folder by (1) right-clicking (or 2-finger click) the following links, (2) clicking `Save Link As`, and (3) selecting your `Week_3` folder.
    - Style 1: <a href="https://gitlab.com/benwang/build_website_course/-/raw/master/Course_Materials/Week_3/style1.css" target="_blank">Right click me and Save Link As</a>
    - Style 2: <a href="https://gitlab.com/benwang/build_website_course/-/raw/master/Course_Materials/Week_3/style2.css" target="_blank">Right click me and Save Link As</a>
3) In your `index.html`, add the following `<link>` line to your `<head>` section. Now open `index.html` in your web browser.

    ```html
    <head>
      ...
      <link rel="stylesheet" href="style1.css">
    </head>
    ```

4) Repeat with style2.css. Which do you like more?
5) Now apply your favorite style to both `index.html` and your other HTML pages.

My example solution is here:
- <a href="https://gitlab.com/benwang/build_website_course/-/raw/master/Course_Materials/Week_3/index.html" target="_blank">index.html</a>
- <a href="https://gitlab.com/benwang/build_website_course/-/raw/master/Course_Materials/Week_3/about.html" target="_blank">about.html</a>

### JavaScript Activity

Links:
- <a href="https://www.w3schools.com/js/tryit.asp?filename=tryjs_intro_inner_html" target="_blank">Changing web page content</a>
- <a href="https://www.w3schools.com/js/tryit.asp?filename=tryjs_dom_animate_3" target="_blank">Animation</a>
- <a href="https://www.w3schools.com/js/js_examples.asp" target="_blank">All examples</a>