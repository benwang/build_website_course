---
layout: post
title:  "Week 4 - Computer Hardware and Google Sites"
date:   2020-06-07
categories: syllabus
---

## Meeting info

We will meet on **Thursdays at 8pm CT/9pm ET** and last for approximately an hour. <b>Link to the class <a href="{{ site.zoomlink }}" target="_blank">Zoom meeting</a></b>.

## Objectives

- Gain basic understanding the basic hardware and software of computers
- Set up your first Google Sites page
- Create some basic content

## During class

- Review material from Weeks 1-3
- Cover basics of computer hardware and software
- Log in to Google sites (https://sites.google.com/)
- Create a new site and select a template

## Homework

- Continue implementing your website on Google Sites

<br>

---

<br>

## Week 4 Material

- Full Week 4 materials: <a href="https://gitlab.com/benwang/build_website_course/-/tree/master/Course_Materials/Week_4" target="_blank">here</a>
- Slides: <a href="https://gitlab.com/benwang/build_website_course/-/raw/master/Course_Materials/Week_4/Week_4_Slides.pdf?inline=false" target="_blank">here</a>

<br>

Steps to creating your Google Site:

1) Go to <a href="http://sites.google.com/" target="_blank">http://sites.google.com/</a> and log in with your Google account.
2) You should arrive on a page like this. Click the first tile on the left, with the "+" and the word "Blank" underneath it.

    ![01]({{ site.baseurl }}/images/Week_4/01_Google_Sites_Home.PNG)

3) Your page should look something like below. The user interface is broken up several parts:

    - The large preview pane on the left
    - The title bar at the top including the name of your website, site settings, and the Publish button
    - The pane on the right with the various elements you can use on your site, a list of your site's pages, and the theme selector.

    ![02]({{ site.baseurl }}/images/Week_4/02_Google_Sites_Blank.PNG)

4) In the right-hand pane, click the **Themes** button at the top. Select a theme that you like. Note that in addition to selecting a theme, you can also customize the color and font used.

    ![03]({{ site.baseurl }}/images/Week_4/03_Google_Sites_Themes.PNG)

5) In my example, I picked the Vision theme with blue color and the default font:

    ![04]({{ site.baseurl }}/images/Week_4/04_Google_Sites_Themes.PNG)

6) Now that you have a theme picked out, let's give your site a title. Click the box at the very top of the page and give a descriptive title.

    ![05]({{ site.baseurl }}/images/Week_4/05_Google_Sites_Site_Name.PNG)

7) Now let's add some pages. In the right-hand pane, click **Pages** at the top.

    ![06]({{ site.baseurl }}/images/Week_4/06_Google_Sites_Pages.PNG)

8) To add a page, click the "+" at the bottom of the page and give your new page a name. Here, I'm adding a "Projects" page

    ![07]({{ site.baseurl }}/images/Week_4/07_Google_Sites_New_Page.PNG)

9) Now to give your page some content. In the right-hand pane, click **Insert**. Here are all of the types of elements you can add to your web pages. You can customize by placing your own elements or selecting one of the default layouts.

    Note that if you scroll down to the bottom of the right-hand pane, there are even more options to choose from (such as collapsible text, charts, forms, maps, and more!)

    ![08]({{ site.baseurl }}/images/Week_4/08_Google_Sites_Insert_tab.PNG)

10) Here I select one of the default layouts to get started.

    ![09]({{ site.baseurl }}/images/Week_4/09_Google_Sites_Insert_content.PNG)

11) And add some pictures and text:

    ![11]({{ site.baseurl }}/images/Week_4/11_Google_Sites_Add_images_text.PNG)

12) When you are ready to publish your site (don't worry, you can always come back and keep editing after publishing), click the **Publish** button on the top-right.

    ![12]({{ site.baseurl }}/images/Week_4/12_Google_Sites_Publish.PNG)

13) Enter a web address in the box. This is just what gets appended to the end of the default Google Site's URL to make *your* URL. Click **Publish** when done (you can change this later too).

    ![13]({{ site.baseurl }}/images/Week_4/13_Google_Sites_Publish_URL.PNG)

14) To view your site, click the arrow next to **Publish** and click **View published site**

    ![14]({{ site.baseurl }}/images/Week_4/14_Google_Sites_Publish_View.PNG)

15) Congratulations! Your site is now live. You can send the link to other people and they can see it too.

    ![14]({{ site.baseurl }}/images/Week_4/15_Google_Sites_Final.PNG)
