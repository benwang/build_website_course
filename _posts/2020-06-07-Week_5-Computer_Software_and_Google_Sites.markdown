---
layout: post
title:  "Week 5 - Computer Software and Google Sites"
date:   2020-06-07
categories: syllabus
---

## Meeting info

We will meet on **Thursdays at 8pm CT/9pm ET**. <b>Link to the class <a href="{{ site.zoomlink }}" target="_blank">Zoom meeting</a></b>.

## Objectives

- Discuss Operating Systems
- Maybe Computer engineering intro?
- Continue working on Google Sites website

## During Class

## Homework

- Finish website (so we can share next week)
- Any other topics you want to learn about your computer or programming? Examples:
  - Programming with Python, C++
  - Arduino
  - ...


## Week 5 Material

- Full Week 5 materials: <a href="https://gitlab.com/benwang/build_website_course/-/tree/master/Course_Materials/Week_5" target="_blank">here</a>
- Slides: <a href="https://gitlab.com/benwang/build_website_course/-/raw/master/Course_Materials/Week_5/Week_5_Slides.pdf?inline=false" target="_blank">here</a>
- Week 4 instructions for Google Sites reference: [here]({{ site.baseurl }}{% post_url 2020-06-07-Week_4-Google_Sites %})