---
layout: post
title:  "Week 6 - Wrap-up"
date:   2020-06-07
categories: syllabus
---

## Meeting info

We will meet on **Thursdays at 8pm CT/9pm ET** and last for approximately an hour. <b>Link to the class <a href="{{ site.zoomlink }}" target="_blank">Zoom meeting</a></b>.

## Objectives

- Review key points from course
- Where to go from here?

## During Class

- Show & tell everyone's websites
  - https://sites.google.com/view/cjsfavoritefood/home
  - http://jnicosia.com
  - https://skatingscheduler.pythonanywhere.com/

## Week 6 Material

- Full Week 6 materials: <a href="https://gitlab.com/benwang/build_website_course/-/tree/master/Course_Materials/Week_6" target="_blank">here</a>
- Slides: <a href="https://gitlab.com/benwang/build_website_course/-/raw/master/Course_Materials/Week_6/Week_6_Slides.pdf?inline=false" target="_blank">here</a>