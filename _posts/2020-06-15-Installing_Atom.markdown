---
layout: post
title:  "Installing Atom"
date:   2020-06-07
categories: computer_basics
---

# Installing Atom
Please follow the following instructions to install Atom based on your operating system.

<details>
    <summary>What is an Operating System?</summary>
    Also known as an OS, the Operating System is the basic software that runs on your computer that allows you to run other applications and connect to other devices, such as the Internet, mouse and keyboard, printers, monitors, etc.
</details>

## Windows

## Mac
<details>
    <summary>Click to expand</summary>
    
1) Go to atom.io and click the large yellow **Download** button

    ![Atom Download Mac]({{ site.baseurl }}/images/Install_Atom/Install_Atom-Mac_download.png)


2) Once the download has completed, click on the **Downloads** icon in your Dock. Then click **Open in Finder**.

    ![Atom Download Mac]({{ site.baseurl }}/images/Install_Atom/Install_Atom-Mac_download_tray.png)

3) This will open up in **Finder**

    ![Finder]({{ site.baseurl }}/images/Install_Atom/Install_Atom-Mac_download_finder.png)

4) Double click to begin extraction of the zip file.

    ![Extracting]({{ site.baseurl }}/images/Install_Atom/Install_Atom-Mac_download_extracting.png)

5) The extracted file will be in your **Downloads** folder

    ![Extracted]({{ site.baseurl }}/images/Install_Atom/Install_Atom-Mac_download_extracted.png)

6) Now click and drag the Atom executable to your **Applications** folder, located in the left sidebar of the **Finder** window.

    ![Installing]({{ site.baseurl }}/images/Install_Atom/Install_Atom-Mac_download_dragging.png)

7) Now Atom is installed and can be accessed via your **Launchpad**.

    ![Installed]({{ site.baseurl }}/images/Install_Atom/Install_Atom-Mac_installed.png)

8) Additionally, you can drag Atom from the **Launchpad** to your Dock so it's easily accessible.

    ![Docked]({{ site.baseurl }}/images/Install_Atom/Install_Atom-Mac_docked.png)

</details>